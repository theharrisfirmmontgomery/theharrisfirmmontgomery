The Harris Firm LLC - Divorce Lawyer and Bankruptcy Attorney in Montgomery, Alabama

The Harris Firm is a law firm that helps individuals throughout Montgomery, AL in the areas of bankruptcy, family law, divorce, probate, and injury. Many of these cases are worked on a retainer basis or contingency fee.

Address: 4144 Carmichael Rd, Suite 5, Montgomery, AL 36106, USA

Phone: 334-782-9938

Website: https://www.theharrisfirmllc.com/montgomerydivorce
